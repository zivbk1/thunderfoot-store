//import products from './products.json';
import dotenv from 'dotenv';
dotenv.config();

const shopID = process.env['PRINTIFY_SHOP'];

export async function get() {
  const url = `https://api.printify.com/v1/shops/${shopID}/products.json`;
  const token = process.env['PRINTIFY_TOKEN'];
  const auth = `Bearer ${token}`
  let products = await fetch(url, {
    method: 'GET',
    headers: {
      "Authorization": auth,
      "User-Agent": "SvelteKit"
    }
  })
  products = await products.json();

  //console.log(products)

	return {
		status: 200,
		body: products
	};
}