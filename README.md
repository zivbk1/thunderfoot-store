SvelteKit and TailwindUI(ecommerce kit) with Printify and Stripe Checkout - a profitable demo.

Stripe integration based on the post by Sean Mullen at https://www.srmullen.com/articles/sveltekit-stripe-integration/

Styling from TailwindUI Ecommerce components - https://tailwindui.com/#product-ecommerce

Products from Printify - 

Flow:
- Create Products in Printify
- Option 1: Pull products from Printify to `products` object in site/localStorage (Add profit margin to prices in this process).
- Option 2: When products are published in Printify, update the product data in Stripe, then pull from stripe for website content.
- Build cart object from selections on website.
- Pass items in cart to Stripe Checkout.
- On sucessful payment(webhook), Create Order in Printify for purchased items.

Tailwind components:
- SlideOver Cart Button: https://tailwindui.com/components/ecommerce/components/shopping-carts#component-ee213165d75da7e921c0bf15f3ab054b
- Shopping Cart Page: https://tailwindui.com/components/ecommerce/components/shopping-carts#component-28f287f64aac61d7f6dbc24f6311d975
- Product Page: https://tailwindui.com/components/ecommerce/components/product-overviews#component-2904df5d10ee9fc81ba07d1ad61a27ca
- Products List: https://tailwindui.com/components/ecommerce/components/product-lists#component-6fbcf064f526c02be04d422925dfa1df
- Filters sidebar: https://tailwindui.com/components/ecommerce/components/category-filters#component-e799517f86cc2fc79bf0eebb45c16eea